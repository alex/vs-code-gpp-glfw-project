COMPILER_FLAGS = -Wall -g -I/usr/include/freetype2 -I/usr/include/libpng16
LINKER_FLAGS = -lGLESv2 -lglfw -lfreetype

OBJ_NAME = main

all:
	$(CXX) main.cpp $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)