#define GLFW_INCLUDE_ES2
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H
FT_Library ft;

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>
#include <thread>

static const GLuint WIDTH = 800;
static const GLuint HEIGHT = 480;
static const GLchar* vertex_shader_source =
    "#version 100\n"
    "attribute vec3 position;\n"
    "varying vec2 TexCoords;\n"
    "uniform mat4 projection;\n"
    "void main() {\n"
    "   gl_Position = vec4(position, 1.0);\n"
    "   TexCoords = vec2(position.z, 1.0);\n"
    "}\n";
static const GLchar* fragment_shader_source =
    "#version 100\n"
    "uniform mediump vec4 ourColor;\n"
    "void main() {\n"
    "   gl_FragColor = ourColor;\n"
    "}\n";
static const GLfloat vertices[] = {
        -0.5f, -0.5f, 0.0f,
        -0.5f,  0.5f, 0.0f,
         0.5f,  0.5f, 0.0f,
         0.5f,  0.5f, 0.0f,
         0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,
};

GLint common_get_shader_program(const char *vertex_shader_source, const char *fragment_shader_source) {
    enum Consts {INFOLOG_LEN = 512};
    GLchar infoLog[INFOLOG_LEN];
    GLint fragment_shader;
    GLint shader_program;
    GLint success;
    GLint vertex_shader;

    /* Vertex shader */
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader);
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex_shader, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
    }

    /* Fragment shader */
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
    glCompileShader(fragment_shader);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragment_shader, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    }

    /* Link shaders */
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shader_program, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
    }

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    return shader_program;
}

void error_callback(GLint error, const char* description)
{
    fprintf(stderr, "ERROR CALLBACK: %s\n", description);
}

void centerWindow(GLFWwindow *window, GLFWmonitor *monitor)
{
    if (!monitor) return;

    const GLFWvidmode* mode = glfwGetVideoMode(monitor);
    if (!mode) return;

    int monitorX, monitorY;
    glfwGetMonitorPos(monitor, &monitorX, &monitorY);

    int W, H;
    glfwGetWindowSize(window, &W, &H);

    glfwSetWindowPos(window,
            monitorX + (mode->width - W) / 2,
            monitorY + (mode->height - H) /2);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    printf("keypress: %d\t%d\t%d\t%d\n", key, scancode, action, mods);
}

static void PrintGLString(GLenum name)
{
    const GLubyte* ret = glGetString(name);
    if (!ret)
        std::printf("Failed to get GL string: %d\n", name);
    else
        std::printf("%s\n", ret);
}

int main(void) {
    GLuint shader_program, vbo;
    GLint pos;
    
    if (FT_Init_FreeType(&ft))
        std::fprintf(stderr, "ERROR::FREETYPE: Could not init FreeType Library\n");
    FT_Face face; const char* ft_path = "/usr/share/fonts/truetype/fonts-bpg-georgian/BPG_Courier_S_GPL&GNU.ttf";
    if (FT_New_Face(ft, ft_path, 0, &face))
        std::fprintf(stderr, "ERROR::FREETYPE: Failed to load font\n");
    std::printf("style_name: %s\n", face->style_name);
    std::printf("family_name: %s\n", face->family_name);
    std::printf("num_glyphs: %d\n", (int) face->num_glyphs);
    std::printf("face_flags: %d\n", (int) face->face_flags);
    std::printf("units_per_EM: %d\n", face->units_per_EM);
    std::printf("num_fixed_sizes: %d\n", face->num_fixed_sizes);
    if (FT_Set_Pixel_Sizes(face, 0, 48))
        fprintf(stderr, "ERROR::FREETYPE: Failed to set font size\n");
    else
        std::cout << "Successfully set font size." << std::endl;
    
    if (FT_Load_Char(face, 'X', FT_LOAD_RENDER))
        std::cout << "ERROR::FREETYPE: Failed to load glyph" << std::endl;

    struct Character {
        GLuint TextureID;
        glm::ivec2 Size;
        glm::ivec2 Bearing;
        GLuint Advance;
    };
    std::map<GLchar, Character> Characters;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    for (GLubyte c = 0; c < 128; ++c) {
        if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
            fprintf(stderr, "ERROR::FREETYPE: Failed to load Glyph\n");
            continue;
        }
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_LUMINANCE,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_LUMINANCE,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer
        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        Character character = {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            face->glyph->advance.x
        };
        Characters.insert(std::pair<GLchar, Character>(c, character));
    }
    FT_Done_Face(face);
    FT_Done_FreeType(ft);


    


    if (!glfwInit()) {
        // Initialization failed
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, __FILE__, nullptr/*glfwGetPrimaryMonitor()*/, nullptr);
    if (!window) {
        // Window or OpenGL context creation failed
    }
    glfwMakeContextCurrent(window);
    centerWindow(window, glfwGetPrimaryMonitor());
    glfwSetKeyCallback(window, key_callback);

    printf("GL_VERSION  : %s\n", glGetString(GL_VERSION) );
    printf("GL_RENDERER : %s\n", glGetString(GL_RENDERER) );

    shader_program = common_get_shader_program(vertex_shader_source, fragment_shader_source);
    pos = glGetAttribLocation(shader_program, "position");


    glClearColor(0.4f, 0.5f, 0.5f, 1.0f);
    glViewport(0, 0, WIDTH, HEIGHT);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(pos);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    printf("GL_VERSION: "); PrintGLString(GL_VERSION);
    printf("GL_RENDERER: "); PrintGLString(GL_RENDERER);
    printf("GL_SHADING_LANGUAGE_VERSION: "); PrintGLString(GL_SHADING_LANGUAGE_VERSION);
    printf("GL_EXTENSIONS: "); PrintGLString(GL_EXTENSIONS);

    double last_time = 0;
    double time;
    unsigned int fps = 0;
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT);
        glUseProgram(shader_program);
        GLint i = glGetUniformLocation(shader_program, "ourColor");
        glUseProgram(shader_program);
        glUniform4f(i, sin(time * 10 + 0.3) / 2 + 0.5f, sin(time * 10) / 2 + 0.5f, cos(time * 10 + 0.3) / 2 + 0.5f, 1);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        std::cout << sin(time) << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(15));
        glfwSwapBuffers(window);

        time = glfwGetTime();
        fps++;
        if (time - last_time > 1.0) {
            printf("%u: %f\n", fps, time);
            fps = 0;
            last_time = time;
        }
    }

    glDeleteBuffers(1, &vbo);
    glfwDestroyWindow(window);
    glfwTerminate();
    return EXIT_SUCCESS;
}
